﻿using Microsoft.Data.Entity;
using MyApp.Models;
using MyApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyApp.Data
{
    /// <summary>
    /// An Entity Framework based implementation of IRepository
    /// </summary>
    public class EntityFrameworkRepository<T> : IRepository<T> where T : BaseModel
    {
        private readonly DbContext _dbContext;
        private DbSet<T> _dbSet;

        public EntityFrameworkRepository(EntityFrameworkDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public T GetById(int id)
        {
            return _dbSet.FirstOrDefault(m => m.Id == id);
        }

        public void Insert(T model)
        {
            var withTimestamps = model as IHasTimestamps;
            if (withTimestamps != null)
            {
                withTimestamps.CreatedAt = DateTime.Now;
                withTimestamps.UpdatedAt = DateTime.Now;
            }

            _dbSet.Add(model);
            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var model = GetById(id);

            if (model != null)
            {
                var withSoftDelete = model as IHasSoftDelete;
                if (withSoftDelete != null)
                {
                    withSoftDelete.DeletedAt = DateTime.Now;
                    Update(model);
                }
                else
                { 
                    _dbSet.Remove(model);
                }
            }
        }

        public void Update(T model)
        {
            // TODO: EF still tracks the object requested before but we are
            // trying to save the one that was bound to the model.
            var withTimestamps = model as IHasTimestamps;
            if (withTimestamps != null)
            {
                withTimestamps.UpdatedAt = DateTime.Now;
            }

            var entry = _dbContext.ChangeTracker.Entry(model);
            entry.State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        #region IDisposable Support
        private bool _isDisposed = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                    // TODO: dispose managed state (managed objects).          
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                _isDisposed = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources. 
        // ~EntityFrameworkRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: tell GC not to call its finalizer when the above finalizer is overridden.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}