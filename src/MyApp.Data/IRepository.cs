﻿using MyApp.Models.Base;
using System;
using System.Collections.Generic;

namespace MyApp.Data
{
    /// <summary>
    /// This interfaces declares the CRUD methods and constraints for our data repository
    /// </summary>
    public interface IRepository<T> : IDisposable where T : BaseModel
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Insert(T model);
        void Delete(int id);
        void Update(T model);
    }
}
