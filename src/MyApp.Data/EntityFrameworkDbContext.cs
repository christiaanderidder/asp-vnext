﻿using System;
using Microsoft.Data.Entity;
using Microsoft.Framework.ConfigurationModel;
using Microsoft.Data.Entity.Metadata;
using MyApp.Models;

namespace MyApp.Data
{
    /// <summary>
    /// Summary description for EntityFrameworkDbContext
    /// </summary>
    public class EntityFrameworkDbContext : DbContext
    {
        private readonly IConfiguration _configuration;

        public EntityFrameworkDbContext(IServiceProvider serviceProvider, IConfiguration configuration) : base(serviceProvider)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptions builder)
        {
            builder.UseSqlServer(_configuration.Get("data:connectionstring"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Post>();
        }
    }
}