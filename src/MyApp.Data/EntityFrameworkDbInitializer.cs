﻿using Microsoft.Data.Entity.SqlServer;
using Microsoft.Framework.DependencyInjection;
using MyApp.Models;
using System;

namespace MyApp.Data
{
    /// <summary>
    /// Initialized the EF database by ensuring it exists and inserting some test data.
    /// Example taken from: https://github.com/aspnet/MusicStore/blob/master/src/MusicStore.Spa/Models/SampleData.cs
    /// </summary>
    public class EntityFrameworkDbInitializer
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            // TODO: Figure out actual seeds and migrations in EF7

            var db = serviceProvider.GetService<EntityFrameworkDbContext>();
            var sqlServerDataStore = db.Configuration.DataStore as SqlServerDataStore;

            if (sqlServerDataStore != null)
            {
                // DB does not exist, create DB
                db.Database.EnsureCreated();
                Seed(db);
            }
            
        }

        private static void Seed(EntityFrameworkDbContext db)
        {
            db.Set<Post>().Add(new Post()
            {
                Title = "Test",
                Content = "Test 12345"
            });
            db.SaveChanges();
        }
    }
}