﻿using Microsoft.AspNet.Mvc;
using MyApp.Services;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyApp.Web.Areas.Controllers.Api
{
    [Area("Api")]
    public class PostsController : Controller
    {
        private IPostService _postService;

        public PostsController(IPostService postService)
        {
            _postService = postService;
        }

        // GET: /<controller>/
        public IActionResult Get()
        {
            return Json(_postService.GetAll());
        }

        // GET: /<controller>/1
        public IActionResult Get(int id)
        {
            var post = _postService.GetById(id);

            if (post == null) return new HttpStatusCodeResult(404);

            return Json(post);
        }

        // POST: /<controller>/
        public IActionResult Post()
        {
            return new HttpStatusCodeResult(201);
        }
    }
}
