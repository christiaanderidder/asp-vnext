﻿using Microsoft.AspNet.Mvc;
using MyApp.Models;
using MyApp.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyApp.Web.Controllers.Web
{
    public class PostsController : Controller
    {
        private IPostService _postService;

        public PostsController(IPostService postService)
        {
            _postService = postService;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View(_postService.GetAll());
        }

        // GET: /<controller>/view/1
        public IActionResult View(int id)
        {
            var post = _postService.GetById(id);

            if (post == null) return new HttpStatusCodeResult(404);

            return View(post);
        }

        // GET: /<controller>/edit/1
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var post = _postService.GetById(id);

            if (post == null) return new HttpStatusCodeResult(404);

            return View(post);
        }

        // POST: /<controller>/edit/1
        [HttpPost]
        public IActionResult Edit(Post post)
        {
            _postService.Update(post);
            return RedirectToAction("view", new { Id = post.Id });
        }
    }
}
