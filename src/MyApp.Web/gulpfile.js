'use strict';
var bower			= require('main-bower-files');	// Processes bower dependencies
var gulp			= require('gulp');				// Gulp
var livereload		= require('gulp-livereload');	// Reload the browser if a change has been detected
var concat			= require('gulp-concat');		// Concatenates assets
var sass			= require('gulp-sass');			// Compiles less
var mincss			= require('gulp-minify-css');	// Minifies css files
var minjs			= require('gulp-uglify');		// Minifies js files
var jshint			= require('gulp-jshint');		// Checks js files for errors
var autoprefixer	= require('gulp-autoprefixer');	// Automatically adds vendor prefixes in css
var ngannotate		= require('gulp-ng-annotate');	// Adds annotations to angular DI so the code can safely be minfied
var filter			= require('gulp-filter');		// Filters piped input

var scriptsGlob = 'Assets/Scripts/**/*.js';
var stylesGlob  = 'Assets/Styles/**/*.scss';


var destination = 'Public/assets/';
var stylesDest = destination + 'css/';
var scriptsDest = destination + 'js/';
var fontsDest = destination + 'fonts/';

gulp.task('gulpfile', function() {
	return gulp.src('gulpfile.js')
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('scripts', function() {
	return gulp.src(scriptsGlob)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(ngannotate())
		.pipe(minjs())
		.pipe(concat('app.js'))
		.pipe(gulp.dest(scriptsDest))
		//.pipe(livereload());
});

gulp.task('styles', function() {
	return gulp.src(stylesGlob)
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(mincss())
		.pipe(concat('app.css'))
		.pipe(gulp.dest(stylesDest))
		//.pipe(livereload());
});


var bowerJsFilter = filter(['**/*.js', '!**/*.min.js']);
var bowerCssFilter = filter(['**/*.css', '!**/*.min.css']);
var bowerFontFilter = filter(['**/*.{eot,svg,ttf,woff}']);

gulp.task('bower', function() {

	return gulp.src(bower())
		.pipe(bowerJsFilter) // Scripts
		.pipe(ngannotate())
		.pipe(minjs())
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest(scriptsDest))
		.pipe(bowerJsFilter.restore())
		
		.pipe(bowerCssFilter) // Css
		.pipe(mincss())
		.pipe(concat('vendor.css'))
		.pipe(gulp.dest(stylesDest))
		.pipe(bowerCssFilter.restore())

		.pipe(bowerFontFilter) // Fonts
		.pipe(gulp.dest(fontsDest))
		.pipe(bowerFontFilter.restore())

		//.pipe(livereload());
});

gulp.task('default', function() {

	// Make sure the tasks run at least once
	gulp.run('bower');
	gulp.run('scripts');
	gulp.run('styles');

	gulp.watch('gulpfile.js', ['gulpfile']);
	gulp.watch('bower.json', ['bower']);
	gulp.watch(scriptsGlob, ['scripts']);
	gulp.watch(stylesGlob, ['styles']);
});

gulp.task('build', function() {
    gulp.run('bower');
    gulp.run('scripts');
    gulp.run('styles');
});