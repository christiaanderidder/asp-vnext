﻿using System;
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Routing;
using MyApp.Core;
using MyApp.Data;
using Microsoft.AspNet.Diagnostics;

namespace MyApp.Web
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940

            app.UseServices(services =>
            {
                services.AddMvc();
                services.AddMyApp();
            });

            app.UseErrorPage(ErrorPageOptions.ShowAll);
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute("ApiRoute", "{area}/{controller}/{id?}");
                routes.MapRoute("WebRoute", "{controller}/{action}/{id?}", new { controller = "Posts", action = "Index" });
            });

            EntityFrameworkDbInitializer.Initialize(app.ApplicationServices);
        }
    }
}
