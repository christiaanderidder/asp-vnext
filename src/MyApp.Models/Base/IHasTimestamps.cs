﻿using System;

namespace MyApp.Models
{
    /// <summary>
    /// This interface adds dates for when the model was created and last updated and makes it recognizable as one that supports timestamps.
    /// </summary>
    public interface IHasTimestamps
    {
	    DateTime CreatedAt { get; set; }
        DateTime UpdatedAt { get; set; }
    }
}