﻿using System;

namespace MyApp.Models
{
    /// <summary>
    /// This interface adds a deleted at field and makes the model recognizable as one that supports soft deleting.
    /// </summary>
    public interface IHasSoftDelete
    {
	    DateTime DeletedAt { get; set; }
    }
}