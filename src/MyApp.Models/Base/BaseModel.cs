﻿using System;

namespace MyApp.Models.Base
{
    /// <summary>
    /// An abstract base class for all models which adds an Id
    /// </summary>
    public abstract class BaseModel
    {
	    public int Id { get; set; }
    }
}