﻿using MyApp.Models.Base;
using System;

namespace MyApp.Models
{
    /// <summary>
    /// A website post
    /// </summary>
    public class Post : BaseModel, IHasTimestamps, IHasSoftDelete
    {
	    public Post()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }

        public string Title { get; set; }
        public string Content { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DeletedAt { get; set; }
    }
}