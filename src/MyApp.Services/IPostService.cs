﻿using MyApp.Models;
using System;
using System.Collections.Generic;

namespace MyApp.Services
{
    /// <summary>
    /// This service is used to fetch Posts
    /// </summary>
    public interface IPostService
    {
        IEnumerable<Post> GetAll();
        Post GetById(int id);
        void Update(Post post);
    }
}
