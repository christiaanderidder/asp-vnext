﻿using MyApp.Data;
using MyApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyApp.Services
{
    public class PostService : IPostService
    {
        private readonly IRepository<Post> _postRepository;

        public PostService(IRepository<Post> postRepository)
        {
            _postRepository = postRepository;
        }

        public IEnumerable<Post> GetAll()
        {
            return _postRepository.GetAll();
        }

        public Post GetById(int id)
        {
            return _postRepository.GetById(id);
        }

        public void Update(Post post)
        {
            _postRepository.Update(post);
        }
    }
}
