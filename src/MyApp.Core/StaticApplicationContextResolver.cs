﻿using System;

namespace MyApp.Core
{
    /// <summary>
    /// This resolver returns a fixed value, until we have the logic in place to resolve from other places
    /// e.g. URL, Computer name, Config files, Cookies, Environment Variables
    /// </summary>
    public class StaticApplicationContextResolver : IApplicationContextResolver
    {
        public ApplicationEnvironment ResolveEnvironment()
        {
            return ApplicationEnvironment.Local;
        }
    }
}