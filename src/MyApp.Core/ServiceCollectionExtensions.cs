﻿using Microsoft.Framework.ConfigurationModel;
using Microsoft.Framework.DependencyInjection;
using MyApp.Data;
using MyApp.Services;
using System.IO;

namespace MyApp.Core
{
    /// <summary>
    /// Registers all dependencies with the ServiceCollection.
    /// </summary>
    public static class ServiceCollectionExtensions
    {
	    public static void AddMyApp(this IServiceCollection services)
	    {
            // Configuration
            var configuration = new Configuration();
            services.AddInstance<IConfiguration>(configuration);

            // Add environment variables to the configuration object.
            configuration.AddEnvironmentVariables();

            var contextResolver = new StaticApplicationContextResolver();
            services.AddInstance<IApplicationContextResolver>(contextResolver);

            // Load our config from a file based on the active environment
            var configurationFile = contextResolver.ResolveEnvironment().ToString().ToLowerInvariant() + ".json";
            configuration.AddJsonFile(configurationFile);           

            // Data
            services.AddEntityFramework().AddSqlServer();
            services.AddTransient<EntityFrameworkDbContext>();
            services.AddTransient(typeof(IRepository<>), typeof(EntityFrameworkRepository<>));

            // Services
            // TODO: Load by namespace convention
            services.AddSingleton<IPostService, PostService>();
        }

        public static void AddPlugins(this IServiceCollection services)
        {
            // TODO: Add dynamically loaded plugins!
        }
    }
}