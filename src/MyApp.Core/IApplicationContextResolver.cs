﻿using System;

namespace MyApp.Core
{
    public interface IApplicationContextResolver
    {
	    ApplicationEnvironment ResolveEnvironment();
    }
}