﻿using System;

namespace MyApp.Core
{
    /// <summary>
    /// Describes the different environments the application can run in.
    /// </summary>
    public enum ApplicationEnvironment
    {
	    Local,
        Staging,
        Production
    }
}